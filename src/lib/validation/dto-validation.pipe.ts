import { ArgumentMetadata, Injectable, PipeTransform } from '@nestjs/common'
import { validate } from 'class-validator'
import { plainToInstance } from 'class-transformer'
import { Type } from '@doesrobbiedream/ts-utils'
import { DtoValidationException } from '@doesrobbiedream/nest-catcher'

@Injectable()
export class DtoValidator implements PipeTransform {
  constructor(protected message?: string) {}

  async transform(value: unknown, { metatype }: ArgumentMetadata) {
    value = this.preProcessValue(value)

    if (!metatype || !this.isTransformable(metatype)) {
      return value
    }
    const object = plainToInstance(metatype, value)
    const errors = await validate(object)
    if (errors.length > 0) {
      throw new DtoValidationException(errors, this.message)
    }
    return value
  }

  preProcessValue(value: unknown) {
    return value
  }

  private isTransformable(metatype: Type<any>): boolean {
    const primitives: Type[] = [String, Boolean, Number, Array, Object]
    return !primitives.includes(metatype)
  }
}
