import { Type } from '@doesrobbiedream/ts-utils'
import { plainToInstance } from 'class-transformer'
import { validate, ValidationError } from 'class-validator'

export interface SuccessfulValidation<T> {
  instance: T
  errors: null
}

export interface FailedResponse {
  instance: null
  errors: ValidationError[]
}

export type ValidationResponse<T> = SuccessfulValidation<T> | FailedResponse

export class CodecValidator {
  public static async validate<T extends object>(
    type: Type<T>,
    values: object
  ): Promise<ValidationResponse<T>> {
    const instance: T = plainToInstance(type, values)
    const errors = await validate(instance)
    if (errors.length) {
      return {
        instance: null,
        errors,
      }
    }
    return {
      instance,
      errors: null,
    }
  }

  public static hasErrors(
    validation: ValidationResponse<unknown>
  ): validation is FailedResponse {
    return !!validation.errors
  }

  public static hasError<T>(
    validation: FailedResponse,
    property: string
  ): boolean {
    return !!validation.errors.find((e) => e.property === property)
  }

  public static hasFailedConstraint(
    validation: FailedResponse,
    property: string,
    constraint: string
  ): boolean {
    return !!validation.errors.find(
      (e) =>
        e.property === property &&
        Object.keys(e.constraints).includes(constraint)
    )
  }
}
