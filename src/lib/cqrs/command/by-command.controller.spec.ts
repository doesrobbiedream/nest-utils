import { CommandsController } from './commands.controller'
import { CommandBus } from '@nestjs/cqrs'
import { EventDrivenAndTransactionalCommand } from './types/event-driven-and-transactional-command.interface'
import { CommandExecutionResponse } from './command-execution-response.class'
import { CommandFailedResponse } from './command-failed.response.class'

class ConcreteByCommandController extends CommandsController {
  constructor(commandBus: CommandBus) {
    super(commandBus)
  }

  test() {
    return this.execute({} as EventDrivenAndTransactionalCommand<unknown>)
  }
}

describe(CommandsController.name, function () {
  describe('when executing a command', function () {
    let controller: ConcreteByCommandController
    let commandBus: CommandBus
    beforeEach(() => {
      commandBus = { execute: jest.fn() } as unknown as CommandBus
      controller = new ConcreteByCommandController(commandBus)
    })
    describe('and successful command execution', function () {
      beforeEach(() => {
        jest
          .spyOn(commandBus, 'execute')
          .mockImplementationOnce(() => Promise.resolve())
      })
      it('should return a CommandExecutionResponse on success', async () => {
        return expect(controller.test()).resolves.toBeInstanceOf(
          CommandExecutionResponse
        )
      })
    })
    describe('and failing command execution', function () {
      beforeEach(() => {
        jest
          .spyOn(commandBus, 'execute')
          .mockImplementationOnce(() => Promise.reject())
      })
      it('should return a CommandFailedResponse on failure', async () => {
        return expect(controller.test()).rejects.toBeInstanceOf(
          CommandFailedResponse
        )
      })
    })
  })
})
