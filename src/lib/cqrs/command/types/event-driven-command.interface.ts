import { ICommand } from '@nestjs/cqrs'

export interface EventDrivenCommand<EventType> extends ICommand {
  event: EventType
}
