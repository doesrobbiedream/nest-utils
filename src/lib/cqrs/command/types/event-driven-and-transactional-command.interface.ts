import { EventDrivenCommand } from './event-driven-command.interface'
import { TransactionalCommand } from './transactional-command.interface'

export type EventDrivenAndTransactionalCommand<EventType> =
  EventDrivenCommand<EventType> & TransactionalCommand
