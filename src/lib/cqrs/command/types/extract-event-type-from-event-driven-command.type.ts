import { EventDrivenCommand } from './event-driven-command.interface'

export type ExtractEventTypeFromEventDrivenCommand<
  C extends EventDrivenCommand<unknown>
> = C extends EventDrivenCommand<infer E> ? E : never
