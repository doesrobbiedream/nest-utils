import { ICommand } from '@nestjs/cqrs'

export interface TransactionalCommand extends ICommand {
  transaction_id: string
}
