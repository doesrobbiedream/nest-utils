import { ICommandHandler } from '@nestjs/cqrs'
import { EventDrivenCommand } from './event-driven-command.interface'

export interface IEventDrivenCommandHandler<
  Command extends EventDrivenCommand<unknown>
> extends ICommandHandler<Command> {
  execute(command: Command): Promise<void>
}
