import { CommandBus } from '@nestjs/cqrs'
import { CommandExecutionResponse } from './command-execution-response.class'
import { CommandFailedResponse } from './command-failed.response.class'
import { Logger } from '@nestjs/common'
import { tap } from '@doesrobbiedream/ts-utils'
import { TransactionalCommand } from './types/transactional-command.interface'

export abstract class CommandsController {
  protected logger: Logger = new Logger()

  protected constructor(protected commander: CommandBus) {}

  protected execute(command: TransactionalCommand) {
    return this.commander
      .execute(command)
      .then(
        tap(() =>
          this.logger.log(
            `Command ${command.constructor.name} executed correctly`
          )
        )
      )
      .then(() => new CommandExecutionResponse(command))
      .catch((e) => {
        this.logger.error(
          `Command ${
            command.constructor.name
          } failed to execute. ${JSON.stringify(e)}`
        )
        throw new CommandFailedResponse(command, e)
      })
  }
}
