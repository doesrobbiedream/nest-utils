import { TransactionalCommand } from './types/transactional-command.interface'

export class CommandExecutionResponse {
  protected message
  protected transaction_id: string

  constructor(command: TransactionalCommand) {
    this.message = `Command ${command.constructor.name} has been successfully executed. Transactional Responses will be addressed to ${command.transaction_id}.`
    this.transaction_id = command.transaction_id
  }
}
