import { CustomException } from '@doesrobbiedream/nest-catcher'
import { TransactionalCommand } from './types/transactional-command.interface'

export class CommandFailedResponse extends CustomException {
  protected exception = 'COMMAND_EXECUTION_FAILED'
  protected message: string
  protected _data: unknown

  constructor(command: TransactionalCommand, origin: unknown) {
    super()
    this.message = `Command ${command.constructor.name} failed to execute. Please, try again or contact support.`
    this._data = origin
  }
}
