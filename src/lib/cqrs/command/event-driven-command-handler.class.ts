import {
  EventDrivenCommand,
  ExtractEventTypeFromEventDrivenCommand,
  IEventDrivenCommandHandler,
} from '@doesrobbiedream/nest-utils'
import { Logger } from '@nestjs/common'

export abstract class EventDrivenCommandHandler<
  C extends EventDrivenCommand<ExtractEventTypeFromEventDrivenCommand<C>>
> implements IEventDrivenCommandHandler<C>
{
  protected logger: Logger = new Logger(EventDrivenCommandHandler.name)

  async execute(command: C): Promise<void> {
    await this.initialize(command)
    await this.apply(command)
    const integrityVerified = await this.verify(command)
    if (!integrityVerified) {
      throw Error()
    }
    await this.commit(command)
    await this.propagate(command)
  }

  protected abstract initialize(command: C): Promise<void>

  protected abstract apply(command: C): Promise<void>

  protected abstract verify(command: C): Promise<boolean>

  protected abstract commit(command: C): Promise<void>

  protected abstract propagate(command: C): Promise<void>
}
