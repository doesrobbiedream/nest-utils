export interface TestSuite {
  describe(...context: any[]): void

  run(...context: any[]): void
}
