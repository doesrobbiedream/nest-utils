import { diff, group } from 'radash'
import {
  EntrypointDefinition,
  FailedScenarioResponse,
} from './by-command-controller.test-suite.types'
import { Type } from '@doesrobbiedream/ts-utils'
import * as request from 'supertest'
import { INestApplication } from '@nestjs/common'
import { Controller } from '@nestjs/common/interfaces'
import { TestSuite } from './test-suite.interface'
import { BoxedTestSuite } from './boxed.test-suite'

declare const describe, it, expect, jest

export class MockedCommandBus {
  execute() {
    throw Error('You should spy and mock implementation of this method')
  }
}

interface ByCommandControllerTestSuiteContext {
  controller: Controller
  app: INestApplication
  entryPointDefinitions: EntrypointDefinition[]
}

interface ByCommandControllerTestSuiteContextArguments {
  app: INestApplication
}

export class ByCommandControllerTestSuite
  extends BoxedTestSuite<
    ByCommandControllerTestSuiteContext,
    ByCommandControllerTestSuiteContextArguments
  >
  implements TestSuite
{
  public static MockedCommandBus = MockedCommandBus

  constructor(
    protected controllerType: Type<Controller>,
    protected entryPointDefinitions: EntrypointDefinition[]
  ) {
    super()
  }

  describe() {
    describe(`when testing entry points for controller ${this.controllerType.name}`, () => {
      it(`should test all entry points`, () => {
        this.whenTriggered.then(({ controller, entryPointDefinitions }) => {
          const entrypointPaths = []
          const controllerPath = Reflect.getMetadata(
            'path',
            controller.constructor
          )

          for (const member of Reflect.ownKeys(
            controller.constructor.prototype
          ).filter((method) => method !== 'constructor')) {
            const accessPointPath = Reflect.getMetadata(
              'path',
              controller[member.toString()]
            )
            if (accessPointPath) {
              entrypointPaths.push(`/${controllerPath}/${accessPointPath}`)
            }
          }
          try {
            expect(entrypointPaths).toEqual(
              entryPointDefinitions.map((data) => data.endpoint)
            )
          } catch (e) {
            throw Error(`
        All controller entry points should have defined test cases. \n
        Missing entrypoints to be tested: \r
        ${diff(e.matcherResult.actual, e.matcherResult.expected).map(
          (controllerPath) => `\t • ${controllerPath} \n`
        )}
      `)
          }
        })
      })
    })
    describe.each(this.entryPointDefinitions)(
      'EntryPoints Automated Tests',
      (entrypoint: EntrypointDefinition) => {
        const { endpoint, command, cases } = entrypoint
        const { SUCCESS, FAILED } = group(
          cases,
          (item) => item.expected_response.status
        )
        describe(`when testing ${endpoint}`, () => {
          it('should have at least one valid case for testing', () => {
            expect(SUCCESS.length).toBeGreaterThan(0)
          })
          describe.each(SUCCESS)(
            'when executed with compliant data',
            (scenario) => {
              const { description, scenario_data } = scenario
              describe(description, () => {
                describe(`and command ${command} executes successfully`, () => {
                  jest
                    .spyOn(MockedCommandBus.prototype, 'execute')
                    .mockImplementationOnce(() => Promise.resolve())
                  it(`should return a command response to ${endpoint} request`, () =>
                    this.assertCommandExecutedSuccessfully(
                      endpoint,
                      scenario_data,
                      command
                    ))
                })
                describe(`and command ${command} failed to execute`, () => {
                  jest
                    .spyOn(MockedCommandBus.prototype, 'execute')
                    .mockImplementationOnce(() => Promise.reject())
                  it(`should return a command exception to ${endpoint} request`, () =>
                    this.assertEntrypointReturnsException(
                      endpoint,
                      scenario_data,
                      'COMMAND_EXECUTION_FAILED'
                    ))
                })
              })
            }
          )
          if (FAILED && FAILED.length) {
            describe.each(FAILED)(
              'when executed with non-compliant data',
              (scenario) => {
                const { description, scenario_data, expected_response } =
                  scenario

                describe(description, () => {
                  it('should return a validation error', () =>
                    this.whenTriggered.then(({ app }) =>
                      this.assertEntrypointReturnsException(
                        endpoint,
                        scenario_data,
                        (expected_response as FailedScenarioResponse).exception
                      )
                    ))
                })
              }
            )
          }
        })
      }
    )
  }

  buildContext({
    app,
  }: ByCommandControllerTestSuiteContextArguments): ByCommandControllerTestSuiteContext {
    return {
      app,
      controller: app.get(this.controllerType),
      entryPointDefinitions: this.entryPointDefinitions,
    }
  }

  protected assertEntrypointReturnsException(
    endpoint,
    failingCase: string | object,
    exception: string
  ) {
    return this.whenTriggered
      .then(({ app }) => {
        return request(app.getHttpServer()).post(endpoint).send(failingCase)
      })
      .then(({ body }) => expect(body.exception).toEqual(exception))
      .catch((e) => expect(e).not.toBeDefined())
  }

  protected assertCommandExecutedSuccessfully(
    endpoint,
    validCase: string | object,
    command: string
  ) {
    return this.whenTriggered
      .then(({ app }) =>
        request(app.getHttpServer()).post(endpoint).send(validCase)
      )
      .then(({ body }) => {
        const message = `Command ${command} has been successfully executed. Transactional Responses will be addressed to`
        expect(body.message).toEqual(expect.stringContaining(message))
      })
      .catch((e) => expect(e).not.toBeDefined())
  }
}
