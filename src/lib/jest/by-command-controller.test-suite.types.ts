export enum ScenarioResponseStatus {
  SUCCESS = 'SUCCESS',
  FAILED = 'FAILED',
}

export interface FailedScenarioResponse {
  status: ScenarioResponseStatus.FAILED
  exception: string
}

export interface SuccessfulScenarioResponse {
  status: ScenarioResponseStatus.SUCCESS
}

export type ExpectedScenarioResponse =
  | SuccessfulScenarioResponse
  | FailedScenarioResponse

export interface ScenarioDefinition<T> {
  description: string
  scenario_data: T
  expected_response: ExpectedScenarioResponse
}

export interface EntrypointDefinition {
  endpoint: string
  command: string
  cases: Array<ScenarioDefinition<any>>
}
