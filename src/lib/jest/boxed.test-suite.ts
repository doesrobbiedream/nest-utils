import { firstValueFrom, Subject } from 'rxjs'
import { TestSuite } from './test-suite.interface'

export abstract class BoxedTestSuite<Context, ContextArguments>
  implements TestSuite
{
  protected trigger$: Subject<Context> = new Subject<Context>()
  protected whenTriggered: Promise<Context> = firstValueFrom(this.trigger$)

  abstract describe(...context: any[]): void

  abstract buildContext(contextArguments: ContextArguments): Context

  run(contextArguments: ContextArguments) {
    this.trigger$.next(this.buildContext(contextArguments))
  }
}
